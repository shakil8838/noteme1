package xyz.xandsoft.noteme.ui.activities

import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import xyz.xandsoft.noteme.R
import xyz.xandsoft.noteme.ui.fragments.open.OpenFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, OpenFragment())
            .commit()
    }
}