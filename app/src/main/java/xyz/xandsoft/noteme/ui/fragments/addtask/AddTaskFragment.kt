package xyz.xandsoft.noteme.ui.fragments.addtask

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ContentValues
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import xyz.xandsoft.noteme.R
import xyz.xandsoft.noteme.data.datamodel.NotesDataModel
import xyz.xandsoft.noteme.data.db.DBHandeler
import xyz.xandsoft.noteme.data.db.DBUtils
import xyz.xandsoft.noteme.utils.showToast
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class AddTaskFragment : Fragment() {

    lateinit var noteTitle: EditText
    lateinit var noteDetails: EditText
    lateinit var dateView: TextView
    lateinit var datePicker: ImageView
    lateinit var status: Spinner
    lateinit var email: LinearLayout
    lateinit var phone: LinearLayout
    lateinit var url: LinearLayout
    lateinit var submitBtn: Button
    lateinit var mDialog: Dialog
    lateinit var mDialogSuccess: Dialog
    lateinit var type: String
    lateinit var id: String

    var getEmail: String = ""
    var getPhone: String = ""
    var getUrl: String = ""
    lateinit var getDeadline: String

    private lateinit var mCalendar: Calendar
    private lateinit var mDatePickerDialListenerog: DatePickerDialog.OnDateSetListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_task, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()

        if (type == "edit") {
            populateData()
        }
    }

    private fun init() {

        type = arguments?.getString("type")!!
        id = arguments?.getString("id")!!

        noteTitle = activity?.findViewById(R.id.add_title)!!
        noteDetails = activity?.findViewById(R.id.add_description)!!
        dateView = activity?.findViewById(R.id.add_task_deadline)!!
        datePicker = activity?.findViewById(R.id.add_task_calender)!!
        status = activity?.findViewById(R.id.add_task_status)!!
        email = activity?.findViewById(R.id.add_task_email)!!
        phone = activity?.findViewById(R.id.add_task_phone)!!
        url = activity?.findViewById(R.id.add_task_url)!!

        mDialog = Dialog(requireActivity())
        mDialog.setContentView(R.layout.layout_dialog_email)

        mDialogSuccess = Dialog(requireActivity())
        mDialogSuccess.setContentView(R.layout.layout_dialog_sucess)

        mCalendar = Calendar.getInstance()
        mDatePickerDialListenerog =
            DatePickerDialog.OnDateSetListener { _, i, i1, i2 ->
                mCalendar.set(Calendar.YEAR, i)
                mCalendar.set(Calendar.MONTH, i1)
                mCalendar.set(Calendar.DAY_OF_MONTH, i2)
                val myFormat = "dd-MM-yy"
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                dateView.text = sdf.format(mCalendar.time)
                getDeadline = sdf.format(mCalendar.time)
            }

        val dialogTV = mDialog.findViewById<EditText>(R.id.dialog_input)
        val dialogIV = mDialog.findViewById<ImageView>(R.id.dialog_icon)
        val dialogBtn = mDialog.findViewById<Button>(R.id.dialog_save)

        email.setOnClickListener {
            dialogTV.setText("")
            dialogIV.setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_email))
            dialogBtn.setOnClickListener {
                getEmail = dialogTV.text.toString().trim()
                mDialog.dismiss()
            }
            mDialog.show()
        }

        phone.setOnClickListener {
            dialogTV.setText("")
            dialogIV.setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_phone))
            dialogBtn.setOnClickListener {
                getPhone = dialogTV.text.toString().trim()
                mDialog.dismiss()
            }
            mDialog.show()
        }

        url.setOnClickListener {
            dialogTV.setText("")
            dialogIV.setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_url))
            dialogBtn.setOnClickListener {
                getUrl = dialogTV.text.toString().trim()
                mDialog.dismiss()
            }
            mDialog.show()
        }


        submitBtn = activity?.findViewById(R.id.add_submit_btn)!!
        submitBtn.setOnClickListener {
            if (type == "edit") {
                updateNote()
            } else {
                insertIntoDB()
            }
        }

        datePicker.setOnClickListener {
            DatePickerDialog(
                requireActivity(), mDatePickerDialListenerog,
                mCalendar[Calendar.YEAR], mCalendar[Calendar.MONTH],
                mCalendar[Calendar.DAY_OF_MONTH]
            ).show()
        }

    }

    private fun insertIntoDB() {

        val title = noteTitle.text.toString().trim()
        val descr = noteDetails.text.toString().trim()

        if (title.isNullOrEmpty() || descr.isNullOrEmpty() || getDeadline.isNullOrEmpty()) {
            activity?.showToast("Please fill the required fields")
            return
        } else {

            val date = Calendar.getInstance().time
            val sdf = SimpleDateFormat("dd-MM-yy")

            DBHandeler(requireActivity()).insertNote(
                NotesDataModel(
                    title,
                    descr,
                    sdf.format(date),
                    getDeadline,
                    status.selectedItem.toString().trim(),
                    getEmail,
                    getPhone,
                    getUrl
                )
            )

            dismissSuccess()
        }

    }

    @SuppressLint("Range")
    private fun populateData() {
        val sql = "SELECT * FROM ${DBUtils.TABLE_NAME} WHERE ${DBUtils.COLUMN_ID} = '$id';"
        val cursor = DBHandeler(requireActivity()).dbRAWQuery(sql)

        if (cursor?.moveToFirst()!!) {
            noteTitle.setText(cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_TITLE)))
            noteDetails.setText(cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_DESCRIPTION)))
            dateView.text = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_DEADLINE))
            when (cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_STATUS))) {
                "Open" -> status.setSelection(0)
                "In-Progress" -> status.setSelection(1)
                "Test" -> status.setSelection(2)
                "Done" -> status.setSelection(3)
            }

            mDialog.setCancelable(false)

            email.setOnClickListener {
                mDialog.findViewById<ImageView>(R.id.dialog_icon)
                    .setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_email))
                mDialog.findViewById<EditText>(R.id.dialog_input)
                    .setText(cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_EMAIL)))
                mDialog.findViewById<Button>(R.id.dialog_save).setOnClickListener {
                    getEmail =
                        mDialog.findViewById<EditText>(R.id.dialog_input).text.toString().trim()
                    mDialog.dismiss()
                }
                mDialog.show()
            }

            phone.setOnClickListener {
                mDialog.findViewById<ImageView>(R.id.dialog_icon)
                    .setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_phone))
                mDialog.findViewById<EditText>(R.id.dialog_input)
                    .setText(cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_PHONE)))
                mDialog.findViewById<Button>(R.id.dialog_save).setOnClickListener {
                    getPhone =
                        mDialog.findViewById<EditText>(R.id.dialog_input).text.toString().trim()
                    mDialog.dismiss()
                }
                mDialog.show()
            }

            url.setOnClickListener {
                mDialog.findViewById<ImageView>(R.id.dialog_icon)
                    .setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_url))
                mDialog.findViewById<EditText>(R.id.dialog_input)
                    .setText(cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_URL)))
                mDialog.findViewById<Button>(R.id.dialog_save).setOnClickListener {
                    getUrl =
                        mDialog.findViewById<EditText>(R.id.dialog_input).text.toString().trim()
                    mDialog.dismiss()
                }
                mDialog.show()
            }
        }
    }

    private fun updateNote() {
        val contentValue = ContentValues()

        contentValue.put(DBUtils.COLUMN_TITLE, noteTitle.text.toString().trim())
        contentValue.put(DBUtils.COLUMN_DESCRIPTION, noteDetails.text.toString().trim())
        contentValue.put(DBUtils.COLUMN_DEADLINE, dateView.text.toString().trim())
        contentValue.put(DBUtils.COLUMN_STATUS, status.selectedItem.toString().trim())
        contentValue.put(DBUtils.COLUMN_EMAIL, getEmail)
        contentValue.put(DBUtils.COLUMN_PHONE, getPhone)
        contentValue.put(DBUtils.COLUMN_URL, getUrl)

        DBHandeler(requireActivity()).updateNote(contentValue, id)

        dismissSuccess()

    }

    private fun dismissSuccess() {
        mDialogSuccess.findViewById<Button>(R.id.success_dismiss).setOnClickListener {
            mDialogSuccess.dismiss()
            activity?.onBackPressed()
        }
        mDialogSuccess.show()
    }
}