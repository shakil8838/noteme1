package xyz.xandsoft.noteme.ui.fragments.details

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import xyz.xandsoft.noteme.R
import xyz.xandsoft.noteme.data.db.DBHandeler
import xyz.xandsoft.noteme.data.db.DBUtils
import xyz.xandsoft.noteme.ui.fragments.addtask.AddTaskFragment
import xyz.xandsoft.noteme.utils.showToast


class DetailsFragment : Fragment() {

    lateinit var noteCreated: TextView
    lateinit var noteStatus: TextView
    lateinit var noteTitle: TextView
    lateinit var noteDetails: TextView
    lateinit var noteDeadLine: TextView

    lateinit var noteEmail: LinearLayout
    lateinit var notePhone: LinearLayout
    lateinit var noteUrl: LinearLayout

    lateinit var noteEditBtn: FloatingActionButton

    lateinit var deleteBtn: ImageView

    private lateinit var noteId: String
    lateinit var getEmail: String
    lateinit var getPhone: String
    lateinit var getUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()

        populateDetails(noteId)
        populateDialog()
    }

    private fun init() {

        noteId = arguments?.getString("id").toString()

        noteCreated = activity?.findViewById(R.id.task_createdat)!!
        noteStatus = activity?.findViewById(R.id.task_status)!!
        noteTitle = activity?.findViewById(R.id.details_title)!!
        noteDetails = activity?.findViewById(R.id.details_task)!!
        noteDeadLine = activity?.findViewById(R.id.details_task_deadline)!!
        noteEmail = activity?.findViewById(R.id.details_task_email)!!
        notePhone = activity?.findViewById(R.id.details_task_phone)!!
        noteUrl = activity?.findViewById(R.id.details_task_url)!!
        deleteBtn = activity?.findViewById(R.id.task_delete)!!
        noteEditBtn = activity?.findViewById(R.id.details_task_edit)!!

        deleteBtn.setOnClickListener {
            DBHandeler(requireActivity()).deleteTask(noteId)
            activity?.onBackPressed()
        }

        noteEditBtn.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("id", noteId)
            bundle.putString("type", "edit")
            val addFragment = AddTaskFragment()
            addFragment.arguments = bundle

            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.container, addFragment)
                ?.addToBackStack("")
                ?.commit()
        }


    }

    @SuppressLint("Range")
    private fun populateDetails(id: String) {

        val sql = "SELECT * FROM ${DBUtils.TABLE_NAME} WHERE ${DBUtils.COLUMN_ID} = '$id';";
        val cursor = DBHandeler(requireActivity()).dbRAWQuery(sql)

        if (cursor?.moveToFirst()!!) {
            noteCreated.text = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_CREATED))
            noteStatus.text = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_STATUS))
            noteTitle.text = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_TITLE))
            noteDetails.text = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_DESCRIPTION))
            noteDeadLine.text = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_DEADLINE))

            getEmail = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_EMAIL))
            getPhone = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_PHONE))
            getUrl = cursor.getString(cursor.getColumnIndex(DBUtils.COLUMN_URL))
        }
    }

    private fun populateDialog() {
        val mDialog = Dialog(requireActivity())
        mDialog.setContentView(R.layout.layout_dialog_email)
        val dialogTV = mDialog.findViewById<EditText>(R.id.dialog_input)
        val dialogIV = mDialog.findViewById<ImageView>(R.id.dialog_icon)
        val dialogBtn = mDialog.findViewById<Button>(R.id.dialog_save)

        dialogTV.isEnabled = false
        activity?.resources?.getColor(R.color.black)?.let { dialogTV.setTextColor(it) }
        dialogBtn.text = "Dismiss"

        noteEmail.setOnClickListener {
            dialogIV.setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_email))
            dialogTV.setText(getEmail)
            dialogBtn.setOnClickListener { mDialog.dismiss() }
            mDialog.show()
        }

        notePhone.setOnClickListener {
            dialogIV.setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_phone))
            dialogTV.setText(getPhone)
            dialogBtn.setOnClickListener { mDialog.dismiss() }
            mDialog.show()
        }

        noteUrl.setOnClickListener {
            dialogIV.setImageDrawable(activity?.resources?.getDrawable(R.drawable.ic_url))
            dialogTV.setText(getUrl)
            dialogBtn.setOnClickListener { mDialog.dismiss() }
            mDialog.show()
        }

    }

}