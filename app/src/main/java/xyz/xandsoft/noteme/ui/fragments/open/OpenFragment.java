package xyz.xandsoft.noteme.ui.fragments.open;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import xyz.xandsoft.noteme.R;
import xyz.xandsoft.noteme.data.datamodel.NotesDataModel;
import xyz.xandsoft.noteme.data.db.DBHandeler;
import xyz.xandsoft.noteme.data.db.DBUtils;
import xyz.xandsoft.noteme.ui.adapters.AdapterSingleTask;
import xyz.xandsoft.noteme.ui.fragments.addtask.AddTaskFragment;

public class OpenFragment extends Fragment {

    private RecyclerView notesLists;
    private FloatingActionButton fab;
    private List<NotesDataModel> notesDataList = new ArrayList<>();

    private LinearLayout taskOpen, taskProgress, taskTest, taskDone;

    private static final String TAG = "OpenFragment";

    public OpenFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_open, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();

        populateOpenNotes();
    }

    private void init() {

        notesLists = getActivity().findViewById(R.id.notes_lists);
        fab = getActivity().findViewById(R.id.new_note);

        notesLists.setLayoutManager(new LinearLayoutManager(getActivity()));

        Fragment fragment = new AddTaskFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "new");
        bundle.putString("id", "0");
        fragment.setArguments(bundle);

        fab.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack("")
                    .commit();
        });

        taskOpen = getActivity().findViewById(R.id.task_open);
        taskProgress = getActivity().findViewById(R.id.task_progress);
        taskTest = getActivity().findViewById(R.id.task_test);
        taskDone = getActivity().findViewById(R.id.task_done);

        taskOpen.setOnClickListener(v -> populateOpenNotes());
        taskProgress.setOnClickListener(v -> populateOpenNotes("In-Progress"));
        taskTest.setOnClickListener(v -> populateOpenNotes("Test"));
        taskDone.setOnClickListener(v -> populateOpenNotes("Done"));

    }

    @SuppressLint("Range")
    private void populateOpenNotes() {

        notesDataList.clear();

        String sql = "SELECT * FROM " + DBUtils.TABLE_NAME + " WHERE " +
                DBUtils.COLUMN_STATUS + " = 'Open';";

        Cursor mCursor = new DBHandeler(getActivity()).dbRAWQuery(sql);

        while (mCursor.moveToNext()) {
            notesDataList.add(new NotesDataModel(
                    mCursor.getInt(mCursor.getColumnIndex(DBUtils.COLUMN_ID)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_TITLE)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_DESCRIPTION)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_CREATED)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_DEADLINE)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_STATUS)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_EMAIL)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_PHONE)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_URL))
            ));
        }
        notesLists.setAdapter(new AdapterSingleTask(getActivity(), notesDataList));
    }

    @SuppressLint("Range")
    private void populateOpenNotes(String type) {

        notesDataList.clear();

        String sql = "SELECT * FROM " + DBUtils.TABLE_NAME + " WHERE " +
                DBUtils.COLUMN_STATUS + " = '" + type + "';";

        Cursor mCursor = new DBHandeler(getActivity()).dbRAWQuery(sql);

        while (mCursor.moveToNext()) {
            notesDataList.add(new NotesDataModel(
                    mCursor.getInt(mCursor.getColumnIndex(DBUtils.COLUMN_ID)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_TITLE)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_DESCRIPTION)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_CREATED)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_DEADLINE)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_STATUS)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_EMAIL)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_PHONE)),
                    mCursor.getString(mCursor.getColumnIndex(DBUtils.COLUMN_URL))

            ));
        }
        notesLists.setAdapter(new AdapterSingleTask(getActivity(), notesDataList));
    }
}
