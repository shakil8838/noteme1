package xyz.xandsoft.noteme.ui.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import xyz.xandsoft.noteme.R
import xyz.xandsoft.noteme.data.datamodel.NotesDataModel
import xyz.xandsoft.noteme.data.db.DBHandeler
import xyz.xandsoft.noteme.ui.activities.MainActivity
import xyz.xandsoft.noteme.ui.fragments.addtask.AddTaskFragment
import xyz.xandsoft.noteme.ui.fragments.details.DetailsFragment

class AdapterSingleTask(
    private val mContext: Context,
    private val noteDataModels: MutableList<NotesDataModel>
) : RecyclerView.Adapter<AdapterSingleTask.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(
                mContext
            ).inflate(R.layout.layout_single_task, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.taskTitle.text = noteDataModels[position].title
        holder.taskCreated.text = noteDataModels[position].createdAt
        holder.taskDeadLine.text = noteDataModels[position].deadline

        holder.taskEditBtn.setOnClickListener {
            val fragment = AddTaskFragment()
            val bundle = Bundle()
            bundle.putString("type", "edit")
            bundle.putString("id", noteDataModels[position].id.toString())
            fragment.arguments = bundle

            val mainActivity = it.context as MainActivity
            mainActivity.supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("")
                .commit()
        }

        holder.taskDelete.setOnClickListener {

            DBHandeler(mContext).deleteTask(noteDataModels[position].id.toString())

            val mainActivity = it.context as MainActivity
            val intent: Intent = mainActivity.intent
            mainActivity.overridePendingTransition(0, 0)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            mainActivity.finish()
            mainActivity.overridePendingTransition(0, 0)
            mContext.startActivity(intent)
        }

        holder.itemView.setOnClickListener {
            val fragment = DetailsFragment()
            val bundle = Bundle()
            bundle.putString("id", noteDataModels[position].id.toString())
            fragment.arguments = bundle

            val mainActivity = it.context as MainActivity
            mainActivity.supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("")
                .commit()
        }
    }

    override fun getItemCount(): Int {
        return noteDataModels.size
    }

    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(
        itemView!!
    ) {
        val taskTitle: TextView = itemView?.findViewById(R.id.single_task_tile)!!
        val taskCreated: TextView = itemView?.findViewById(R.id.single_created_date)!!
        val taskDeadLine: TextView = itemView?.findViewById(R.id.single_deadline)!!
        val taskEditBtn: ImageView = itemView?.findViewById(R.id.single_edit)!!
        val taskDelete: ImageView = itemView?.findViewById(R.id.single_delete)!!
    }
}