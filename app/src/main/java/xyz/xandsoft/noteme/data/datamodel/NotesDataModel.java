package xyz.xandsoft.noteme.data.datamodel;

public class NotesDataModel {
    private int id;
    private String title;
    private String description;
    private String createdAt;
    private String deadline;
    private String status;
    private String email;
    private String phone;
    private String url;

    public NotesDataModel(int id, String title, String description, String createdAt,
                          String deadline, String status, String email, String phone, String url) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.createdAt = createdAt;
        this.deadline = deadline;
        this.status = status;
        this.email = email;
        this.phone = phone;
        this.url = url;
    }

    public NotesDataModel(String title, String description, String createdAt, String deadline,
                          String status, String email, String phone, String url) {
        this.title = title;
        this.description = description;
        this.createdAt = createdAt;
        this.deadline = deadline;
        this.status = status;
        this.email = email;
        this.phone = phone;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
