package xyz.xandsoft.noteme.data.db

object DBUtils {

    const val DATABASE_NAME = "xyz.xandsoft.noteme.db"
    const val TABLE_NAME = "notes"

    const val COLUMN_ID = "id"
    const val COLUMN_TITLE = "title"
    const val COLUMN_DESCRIPTION = "description"
    const val COLUMN_CREATED = "created_at"
    const val COLUMN_DEADLINE = "deadline"
    const val COLUMN_STATUS = "status"
    const val COLUMN_EMAIL = "email"
    const val COLUMN_PHONE = "phone"
    const val COLUMN_URL = "url"
}