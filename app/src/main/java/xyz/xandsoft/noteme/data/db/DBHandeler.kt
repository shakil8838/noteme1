package xyz.xandsoft.noteme.data.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import xyz.xandsoft.noteme.data.datamodel.NotesDataModel

class DBHandeler(private val context: Context) : SQLiteOpenHelper(
    context,
    DBUtils.DATABASE_NAME,
    null,
    1
) {

    override fun onCreate(sqliteDB: SQLiteDatabase?) {

        val query = "CREATE TABLE ${DBUtils.TABLE_NAME}(" +
                "${DBUtils.COLUMN_ID} INTEGER PRIMARY KEY NOT NULL, " +
                "${DBUtils.COLUMN_TITLE} VARCHAR(50)," +
                "${DBUtils.COLUMN_DESCRIPTION} TEXT," +
                "${DBUtils.COLUMN_CREATED} VARCHAR(15)," +
                "${DBUtils.COLUMN_DEADLINE} VARCHAR(15)," +
                "${DBUtils.COLUMN_STATUS} VARCHAR(20)," +
                "${DBUtils.COLUMN_EMAIL} VARCHAR(150)," +
                "${DBUtils.COLUMN_PHONE} VARCHAR(15)," +
                "${DBUtils.COLUMN_URL} TEXT);"

        sqliteDB?.execSQL(query)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {

    }

    fun dbRAWQuery(query: String?): Cursor? {
        val mdb = this.writableDatabase
        return mdb.rawQuery(query, null)
    }

    fun insertNote(noteDataModel: NotesDataModel) {
        val sqlite = this.writableDatabase
        val contentValue = ContentValues()

        contentValue.put(DBUtils.COLUMN_TITLE, noteDataModel.title)
        contentValue.put(DBUtils.COLUMN_DESCRIPTION, noteDataModel.description)
        contentValue.put(DBUtils.COLUMN_CREATED, noteDataModel.createdAt)
        contentValue.put(DBUtils.COLUMN_DEADLINE, noteDataModel.deadline)
        contentValue.put(DBUtils.COLUMN_STATUS, noteDataModel.status)
        contentValue.put(DBUtils.COLUMN_EMAIL, noteDataModel.email)
        contentValue.put(DBUtils.COLUMN_PHONE, noteDataModel.phone)
        contentValue.put(DBUtils.COLUMN_URL, noteDataModel.url)

        sqlite.insert(
            DBUtils.TABLE_NAME,
            null,
            contentValue
        )
    }

    fun getNotes(): Cursor {

        val sqlite = this.readableDatabase
        val columns = arrayOf<String>(
            DBUtils.COLUMN_ID,
            DBUtils.COLUMN_TITLE,
            DBUtils.COLUMN_DESCRIPTION,
            DBUtils.COLUMN_CREATED,
            DBUtils.COLUMN_DEADLINE,
            DBUtils.COLUMN_STATUS,
            DBUtils.COLUMN_EMAIL,
            DBUtils.COLUMN_PHONE,
            DBUtils.COLUMN_URL
        )

        return sqlite.query(
            DBUtils.TABLE_NAME,
            columns,
            null,
            null,
            null,
            null,
            null
        )
    }

    fun deleteTask(taskId: String) {
        val mSqLiteDatabase = this.writableDatabase
        mSqLiteDatabase.delete(
            DBUtils.TABLE_NAME,
            DBUtils.COLUMN_ID + " = " + taskId,
            null
        )
    }

    fun updateNote(values: ContentValues, noteID: String) {
        val mSqLiteDatabase = this.writableDatabase
        mSqLiteDatabase.update(
            DBUtils.TABLE_NAME,
            values,
            DBUtils.COLUMN_ID + " = " + noteID,
            null
        )
    }


}